/*
 * types.h
 *
 *  Created on: 10 dec 2014
 *      Author: Fredrik
 */

#ifndef TYPES_H_
#define TYPES_H_

typedef unsigned       char uint8;
typedef                char sint8;
typedef unsigned short int  uint16;
typedef          short int  sint16;
typedef unsigned long       uint32;
typedef          long       sint32;
typedef unsigned long long  uint64; 
typedef          long long  sint64;

typedef uint8 boolean;

#define TRUE (0 == 0)
#define FALSE !TRUE

#endif /* TYPES_H_ */
