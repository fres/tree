/*
 * mcu.h
 *
 *  Created on: Nov 18, 2018
 *      Author: fredrik
 */

#ifndef _MCU_H_
#define _MCU_H_

#include "types.h"

typedef enum
{
	LED_OFF,
	LED_ON,
	LED_TOGGLE,
}DebugLedState;

uint8 getResetCause(void);
void clearResetRegister(void);
void initModePin(void);
void initEnergizePin(void);
void energizeOn(void);
void energizeOff(void);
void debug_led_init(void);
void debug_led_set(DebugLedState level);
boolean readDebugPin(void);
void short_delay(void);
void medium_delay(void);
void long_delay(void);
void delay(volatile uint32 cnt);

uint32 getSystemClockFrequency(void);
uint32 getAHBFrequency(void);
uint32 getAPB1Frequency(void);
uint32 getAPB2Frequency(void);
uint32 getAPB2Prescaler(void);
uint32 getAPB1Prescaler(void);

#endif /* _MCU_H_ */
