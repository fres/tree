#include "types.h"

void initLEDs(void);
void setLED(uint16 index, boolean in);
uint16 getNbrOfLEDs(void);
void initButton(void);
boolean isButtonPressed(void);
