/*
 * mcu.c
 *
 *  Created on: Nov 18, 2018
 *      Author: fredrik
 */
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "types.h"
#include "mcu.h"

#define SHORT_DELAY   (1000000U)
#define MEDIUM_DELAY  (6000000U)
#define LONG_DELAY   (15000000U)

uint8 getResetCause(void)
{
	return RCC_GetFlagStatus(RCC_FLAG_PORRST);
}

void clearResetRegister(void)
{
}

void delay(volatile uint32 cnt)
{
	while (--cnt)
	{
		//compareStatistics();
	}
}

void short_delay(void)
{
	delay(SHORT_DELAY);
}
void medium_delay(void)
{
	delay(MEDIUM_DELAY);
}

void long_delay(void)
{
	delay(LONG_DELAY);
}

uint32 getPLL_PLLN(void)
{
	uint32 plln = ((RCC->PLLCFGR & RCC_PLLCFGR_PLLN) >> 6U);
	return plln;
}
uint32 getPLL_PLLM(void)
{
	uint32 pllm = (RCC->PLLCFGR & RCC_PLLCFGR_PLLM);
	return pllm;
}

uint32 getSystemClockFrequency(void)
{
	uint32 pllm, plln, pllp;
	uint32 hse, pllon;
	uint32 vcoFreq, pllFreq;
	uint32 pllPDivide;
	const uint32 ocsFreq = 8000000U;
	
	pllm = (RCC->PLLCFGR & RCC_PLLCFGR_PLLM);
	plln = ((RCC->PLLCFGR & RCC_PLLCFGR_PLLN) >> 6U);
	pllp = ((RCC->PLLCFGR & RCC_PLLCFGR_PLLP) >> 16U);
	switch(pllp)
	{
		case 0b00:
        pllPDivide = 2U;
		break;
		case 0b01:
		pllPDivide = 4U;
		break;
		case 0x10:
		pllPDivide = 6U;
		break;
		case 0b11:
		pllPDivide = 8U;
		break;
	}
	
    hse  = ((RCC->CR & RCC_CR_HSEON) >> 16U);
	pllon = ((RCC->CR & RCC_CR_PLLON) >> 24U);

	if (hse && pllon)
	{
       vcoFreq = ocsFreq * plln / pllm;
	   pllFreq = vcoFreq / pllPDivide;
	}
	else
	{
		/* Problem, external oscillator not configured! */
		pllFreq = 0U;
	}

	return pllFreq;
}

uint32 getAHBFrequency(void)
{
	uint32 hpre; 
	uint32 ahbPrescaler;
	const uint32 sysClock = getSystemClockFrequency();

    hpre  = ((RCC->CFGR & RCC_CFGR_HPRE) >> 4U);
	switch(hpre)
	{
		case 0x0:
		case 0x1:
		case 0x2:
		case 0x3:
		case 0x4:
		case 0x5:
		case 0x6:
		case 0x7:
		ahbPrescaler = 1U;
		break;

		case 0x8:
		ahbPrescaler = 2U;
		break;

		case 0x9:
		ahbPrescaler = 4U;
		break;

		case 0xA:
		ahbPrescaler = 8U;
		break;

		case 0xB:
		ahbPrescaler = 16U;
		break;

		case 0xC:
		ahbPrescaler = 64U;
		break;

		case 0xD:
		ahbPrescaler = 128U;
		break;

		case 0xE:
		ahbPrescaler = 256U;
		break;

		case 0xF:
		ahbPrescaler = 512;
		break;

	}
    return sysClock / ahbPrescaler;
}

uint32 getAPB1Prescaler(void)
{
	uint32 ppre1;
	uint32 apb1Prescaler;

	ppre1 = ((RCC->CFGR & RCC_CFGR_PPRE1) >> 10U);
	switch(ppre1)
	{
		case 0b000:
		case 0b001:
		case 0b010:
		case 0b011:
		apb1Prescaler = 1U;
		break;
		
		case 0b100:
		apb1Prescaler = 2U;
		break;

		case 0b101:
		apb1Prescaler = 4U;
		break;

		case 0b110:
		apb1Prescaler = 8U;
		break;

		case 0b111:
		apb1Prescaler = 16U;
		break;
	}
    return apb1Prescaler;
}

uint32 getAPB1Frequency(void)
{
	const uint32 ahbFreq = getAHBFrequency();

	return ahbFreq / getAPB1Prescaler();
}

uint32 getAPB2Prescaler(void)
{
	uint32 ppre2;
	uint32 apb2Prescaler;
	ppre2 = ((RCC->CFGR & RCC_CFGR_PPRE2) >> 13U);

	switch(ppre2)
	{
		case 0b000:
		case 0b001:
		case 0b010:
		case 0b011:
        apb2Prescaler = 1U;
		break;

		case 0b100:
		apb2Prescaler = 2U;
		break; 

		case 0b101:
		apb2Prescaler = 4U;
		break; 

		case 0b110:
		apb2Prescaler = 8U;
		break; 

		case 0b111:
		apb2Prescaler = 16U;
		break; 
	}
	return apb2Prescaler;
}

uint32 getAPB2Frequency(void)
{
	const uint32 ahbFreq = getAHBFrequency();

	return ahbFreq / getAPB2Prescaler();
}


