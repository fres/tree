/*
 * types.h
 *
 *  Created on: 29 aug. 2018
 *      Author: t062505
 */

#ifndef TYPES_H_
#define TYPES_H_

/* Basic types for stm32 target. */
typedef unsigned int uint32;
typedef          int sint32;

typedef unsigned short int uint16;
typedef          short int sint16;

typedef unsigned char uint8;
typedef          char sint8;

typedef uint8 boolean;

#define FALSE (0 != 0)
#define TRUE (!FALSE)

typedef float         float32;

#endif /* TYPES_H_ */
