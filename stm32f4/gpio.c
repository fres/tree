#include "gpio.h"
#include "types.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"

typedef struct 
{
    GPIO_TypeDef* port;
    uint16_t pin;
}gpioType;

/* Free pins available for gpio on STM32F4 Discovery board. 
   This table can be used to alter the relation between the
   bits in the pattern and the LED's by changing the order
   in the table. */
gpioType ioTable[] = 
{
   [0] = {.port = GPIOE, .pin = GPIO_Pin_14},
   [1] = {.port = GPIOA, .pin = GPIO_Pin_1},
   [2] = {.port = GPIOA, .pin = GPIO_Pin_2},
   [3] = {.port = GPIOA, .pin = GPIO_Pin_3},
   [4] = {.port = GPIOA, .pin = GPIO_Pin_8},
   [5] = {.port = GPIOA, .pin = GPIO_Pin_15},
   
   [6] = {.port = GPIOB, .pin = GPIO_Pin_0},
   [7] = {.port = GPIOB, .pin = GPIO_Pin_1},
   [8] = {.port = GPIOB, .pin = GPIO_Pin_3},
   [9] = {.port = GPIOB, .pin = GPIO_Pin_4},
  [10] = {.port = GPIOB, .pin = GPIO_Pin_5},
  [11] = {.port = GPIOB, .pin = GPIO_Pin_7},
  [12] = {.port = GPIOB, .pin = GPIO_Pin_8},
  [13] = {.port = GPIOB, .pin = GPIO_Pin_11},
  [14] = {.port = GPIOB, .pin = GPIO_Pin_12},
  [15] = {.port = GPIOB, .pin = GPIO_Pin_13},
  [16] = {.port = GPIOB, .pin = GPIO_Pin_14},
  [17] = {.port = GPIOB, .pin = GPIO_Pin_15},

  [18] = {.port = GPIOC, .pin = GPIO_Pin_1},
  [19] = {.port = GPIOC, .pin = GPIO_Pin_2},
  [20] = {.port = GPIOC, .pin = GPIO_Pin_4},
  [21] = {.port = GPIOC, .pin = GPIO_Pin_5},
  [22] = {.port = GPIOC, .pin = GPIO_Pin_9},
  [23] = {.port = GPIOC, .pin = GPIO_Pin_11},
  [24] = {.port = GPIOC, .pin = GPIO_Pin_13},
  [25] = {.port = GPIOC, .pin = GPIO_Pin_14},
  [26] = {.port = GPIOC, .pin = GPIO_Pin_15},

  [27] = {.port = GPIOD, .pin = GPIO_Pin_0},
  [28] = {.port = GPIOD, .pin = GPIO_Pin_1},
  [29] = {.port = GPIOD, .pin = GPIO_Pin_2},
  [30] = {.port = GPIOD, .pin = GPIO_Pin_3},
  [31] = {.port = GPIOD, .pin = GPIO_Pin_6},
  [32] = {.port = GPIOD, .pin = GPIO_Pin_7},
  [33] = {.port = GPIOD, .pin = GPIO_Pin_8},
  [34] = {.port = GPIOD, .pin = GPIO_Pin_9},
  [35] = {.port = GPIOD, .pin = GPIO_Pin_10},
  [36] = {.port = GPIOD, .pin = GPIO_Pin_11},

  [37] = {.port = GPIOE, .pin = GPIO_Pin_2},
  [38] = {.port = GPIOE, .pin = GPIO_Pin_4},
  [39] = {.port = GPIOE, .pin = GPIO_Pin_5},
  [40] = {.port = GPIOE, .pin = GPIO_Pin_6},
  [41] = {.port = GPIOE, .pin = GPIO_Pin_7},
  [42] = {.port = GPIOE, .pin = GPIO_Pin_8},
  [43] = {.port = GPIOE, .pin = GPIO_Pin_9},
  [44] = {.port = GPIOE, .pin = GPIO_Pin_10},
  [45] = {.port = GPIOE, .pin = GPIO_Pin_11},
  [46] = {.port = GPIOE, .pin = GPIO_Pin_12},
  [47] = {.port = GPIOE, .pin = GPIO_Pin_13},
  //[48] = {.port = GPIOE, .pin = GPIO_Pin_15},
};

void initButton(void)
{
    GPIO_InitTypeDef buttonInit;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

    buttonInit.GPIO_Pin   = GPIO_Pin_0;
    buttonInit.GPIO_Mode  = GPIO_Mode_IN;
    buttonInit.GPIO_OType = GPIO_OType_PP;
    buttonInit.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(GPIOA, &buttonInit);
}

boolean isButtonPressed(void)
{
    if (Bit_RESET == GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0))
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

void initLEDs(void)
{
	GPIO_InitTypeDef        gpioInit;
    uint32 i;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
    
	gpioInit.GPIO_Mode = GPIO_Mode_OUT;
	gpioInit.GPIO_OType = GPIO_OType_PP;
	gpioInit.GPIO_Speed = GPIO_Medium_Speed;
	gpioInit.GPIO_PuPd = GPIO_PuPd_NOPULL;
    for (i = 0; i < (sizeof(ioTable)/sizeof(ioTable[0])); i++)
    {
        gpioInit.GPIO_Pin = ioTable[i].pin;
        GPIO_Init(ioTable[i].port, &gpioInit);
    }
}

void port_led_set(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, boolean in)
{
    if (in)
    {
        GPIO_WriteBit(GPIOx, GPIO_Pin, Bit_RESET);
    }
    else
    {
       GPIO_WriteBit(GPIOx, GPIO_Pin, Bit_SET);
    }
}

void setLED(uint16 index, boolean in)
{
    const gpioType *gpioPtr = &ioTable[index];
    port_led_set(gpioPtr->port, gpioPtr->pin, in);
}

uint16 getNbrOfLEDs(void)
{
    return (uint16)(sizeof(ioTable)/sizeof(ioTable[0]));
}

