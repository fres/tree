/*
 * criticalsection.h
 *
 *  Created on: Dec 13, 2018
 *      Author: fredrik
 */

#ifndef STM32_CRITICALSECTION_H_
#define STM32_CRITICALSECTION_H_
#include "stm32f4xx.h"

#define DISABLE_INTERRUPTS() __disable_irq()
#define ENABLE_INTERRUPTS()  __enable_irq();

#endif /* STM32_CRITICALSECTION_H_ */
