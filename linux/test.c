/*
 * test.c
 *
 * Testing plate to try out things before loading into target...
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

/* SUT */
#include "types.h"
#include "christmastree.h"
#include "gpio.h"
#include "mcu.h"

/* Minimalist test framework. */
uint32_t nbrOfFaults = 0, nbrOfTests;

void test_assert_func (uint8_t cond, int line, char *filePtr)
{
	if (!cond){
		/* Test case failed! */
		nbrOfFaults++;
		nbrOfTests++;
		printf("Test case failed! Line: %d\n", line);
	}
	else
	{
		nbrOfTests++;
	}
}

void test_summary (void)
{
	printf("%d tests executed, %d failed. See above. \n", nbrOfTests, nbrOfFaults);
}
#define test_assert(cond)  (test_assert_func (cond, __LINE__, __FILE__))


void sei(void)
{

}
void cli(void)
{

}

uint8 getResetCause(void)
{
	return 0u;
}
void clearResetRegister(void)
{

}


void delay(uint32 t)
{
	(void)t;
}

void short_delay(void)
{
}
void medium_delay(void)
{
}

void long_delay(void)
{
}

void initButton(void)
{

}
boolean isButtonPressed(void)
{
	return FALSE;
}

uint32 getSystemClockFrequency(void)
{
	return 168000000U;
}

uint32 getAHBFrequency(void)
{
	return 168000000U;
}
uint32 getAPB1Frequency(void)
{
	return 42000000U;
}

uint32 getAPB2Frequency(void)
{
	return 84000000U;
}

uint16 setLED_index;
boolean setLED_in;
void setLED(uint16 index, boolean in)
{
	setLED_index = index;
	setLED_in    = in;
}

void initLEDs(void)
{

}

uint16 getNbrOfLEDs(void)
{
	return 48U;
}


void test_types(void)
{
	test_assert(8 == sizeof(uint64));
	test_assert(4 == sizeof(uint32));
	test_assert(2 == sizeof(uint16));
	test_assert(1 == sizeof(uint8));
}


void test_gpio(void)
{
	test_assert(FALSE == getLEDState(0x0000000000000000, 0));
	test_assert(TRUE  == getLEDState(0x0000000000000001, 0));

	test_assert(FALSE == getLEDState(0x0000000000000000, 1));
	test_assert(TRUE  == getLEDState(0x0000000000000002, 1));

	test_assert(FALSE == getLEDState(0x0000000000000000, 2));
	test_assert(TRUE  == getLEDState(0x0000000000000004, 2));

	test_assert(FALSE == getLEDState(0x0000000000000000, 3));
	test_assert(TRUE  == getLEDState(0x0000000000000008, 3));

    test_assert(FALSE == getLEDState(0x0000000000000000, 47));
	test_assert(TRUE  == getLEDState(0x0000800000000000, 47));

    test_assert(FALSE == getLEDState(0x0000000000000000, 48));
	test_assert(TRUE  == getLEDState(0x0001000000000000, 48));
}

void test_remap(void)
{
	uint32 i;

	test_assert(0x20000000000U  == remapLED(12U));
	test_assert(0U              == remapLED(60U));
	test_assert(0x200000000000U == remapLED(5U));

	for (i = 1U; i <= 48U; i++)
	{
		test_assert(0x0U != remapLED(i));
	}
}


/* Start of test program. */
int main(int argc, char **argv)
{
	test_types();
	test_gpio();
	test_remap();
	runTheTree();

	/* Write the test results.. */
    test_summary();
	return 0;
}

