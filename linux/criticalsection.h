/*
 * criticalsection.h
 *
 *  Created on: Dec 13, 2018
 *      Author: fredrik
 */

#ifndef LINUX_CRITICALSECTION_H_
#define LINUX_CRITICALSECTION_H_

#define DISABLE_INTERRUPTS()
#define ENABLE_INTERRUPTS()

#endif /* LINUX_CRITICALSECTION_H_ */
