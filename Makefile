ifeq (,$(filter obj%,$(notdir $(CURDIR)))) 
include target.mk 
else 
#----- End Boilerplate 

TARGET = tree

ifeq ($(_ARCH),stm32f4)
CFG_ARCH_STM32F4 := y
OUTPUT = $(TARGET).srec
else
ifeq ($(_ARCH),stm32)
CFG_ARCH_STM32 := y
OUTPUT = $(TARGET).elf
else
ifeq ($(_ARCH),linux)
CFG_ARCH_LINUX := y
OUTPUT = $(TARGET).aout
else
ifeq ($(_ARCH),atmega)
CFG_ARCH_ATMEGA := y
OUTPUT = $(TARGET).hex
endif
endif
endif
endif

# all
.SECONDARY:
all: $(OUTPUT)

test: $(OUTPUT)
	../obj_linux/$(OUTPUT)

.PHONY:
clean:

# App files
inc-y = $(SRCDIR)
inc-$(CFG_ARCH_ATMEGA)  += $(SRCDIR)/atmega
inc-$(CFG_ARCH_LINUX)   += $(SRCDIR)/linux
inc-y                   += $(SRCDIR)/include
inc-$(CFG_ARCH_STM32F4) += $(SRCDIR)/stm32f4
INC = $(inc-y)
VPATH += $(SRCDIR)
VPATH += $(SRCDIR)/$(_ARCH)

# ST standard peripheral library
STD_PERIPH_LIB = $(SRCDIR)/../downloads/STM32F4xx_DSP_StdPeriph_Lib_V1.8.0/Libraries
VPATH += $(STD_PERIPH_LIB)/STM32F4xx_StdPeriph_Driver/src
VPATH += $(STD_PERIPH_LIB)/CMSIS/Device/ST/STM32F4xx/Source/Templates/gcc_ride7

# Device selection
DEF += STM32F40_41xxx

# System clock settings
DEF += HSE_VALUE=8000000U

DEF += USE_STDPERIPH_DRIVER

# Use asserts to trap errors.
DEF += USE_FULL_ASSERT

# Application files
obj-y                    += christmastree.o
obj-$(CFG_ARCH_STM32F4)  += main.o
obj-$(CFG_ARCH_STM32F4)  += gpio.o

obj-$(CFG_ARCH_ATMEGA)   += mcu.o
obj-$(CFG_ARCH_STM32F4)  += mcu.o

obj-$(CFG_ARCH_STM32F4)  += stm32f4xx_it.o
obj-$(CFG_ARCH_STM32F4)  += system_stm32f4xx.o
obj-$(CFG_ARCH_STM32F4)  += startup_stm32f40xx.o

# ST driver files
obj-$(CFG_ARCH_STM32F4) += stm32f4xx_rcc.o
obj-$(CFG_ARCH_STM32F4) += stm32f4xx_gpio.o
obj-$(CFG_ARCH_STM32F4) += misc.o
obj-$(CFG_ARCH_STM32F4) += assert.o
obj-$(CFG_ARCH_STM32F4) += syscalls.o

# Host tests
obj-$(CFG_ARCH_LINUX) += test.o
 
$(TARGET).elf: $(obj-y) Makefile rules_gcc_$(_ARCH).mk

# List defines here
INC += $(STD_PERIPH_LIB)/STM32F4xx_StdPeriph_Driver/inc
INC += $(STD_PERIPH_LIB)/CMSIS/Device/ST/STM32F4xx/Include
INC += $(STD_PERIPH_LIB)/CMSIS/Include

# List include directories here

# Linker script
LDSCRIPT += $(SRCDIR)/stm32f4/stm32_flash.ld
LDINC += $(SRCDIR)

# Dependancy handling
-include $(patsubst %.o,%.d,$(obj-y))

# Tool invocations
include $(SRCDIR)/rules_gcc_$(_ARCH).mk
	
#----- Begin Boilerplate 
endif