INCLUDE_DIRS = -L $(LDINC)

Q ?= @
 
CC = $(CC_PATH)gcc

CXX = $(CC_PATH)g++
CXXFLAGS = $(COMPILE_OPTS) $(INCLUDE_DIRS)

AS = $(CC_PATH)gcc
ASFLAGS = $(COMPILE_OPTS) -c

CSTANDARD = -std=gnu99
	
CFLAGS += $(CSTANDARD)
CFLAGS += -O0 -g3 -Wall -c -MMD $(addprefix -I,$(INC)) $(addprefix -D,$(DEF))

LD = $(CC_PATH)gcc
LDFLAGS = $(INCLUDE_DIRS) $(LIBRARY_DIRS)

#
%.o: %.s Makefile $(LDSCRIPT)
	@echo 'Compiling $(notdir $<)'
	$(Q)$(CC) $(CFLAGS) -o$@ $<

%.o: %.c Makefile $(LDSCRIPT)
	@echo 'Compiling $(notdir $<)'
	$(Q)$(CC) $(CFLAGS) -o$@ $<
	
%.aout: $(obj-y)  
	@echo 'Linking $@'
	$(Q)$(CC) $(LDFLAGS) $(obj-y) --output $@ 
	