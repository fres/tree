/*
 * christmastree.c
 *
 *  Created on: 10 dec 2014
 *      Author: Fredrik
 */

#include <stdio.h>
#include <stdlib.h>
#include "mcu.h"
#include "gpio.h"
#include "types.h"

enum{
	STAR_BITS      = 0x00000000000000FF,
	FIRST_STAR_BIT = 0x0000000000000001,
	LAST_STAR_BIT  = 0x0000000000000080,
	TREE_BITS      = 0x0001FFFFFFFFFF00,
	FIRST_TREE_BIT = 0x0000000000000100,
	LAST_BIT       = 0x0002000000000000
};
uint64 ledbitmask;

typedef struct
{
	uint64 bit;
	uint16 index;
}remapType;

const remapType remapTable[] =
{
	[0] = {.bit = 0x1U, .index = 37},
	[1] = {.bit = 0x2U, .index = 15},
	[2] = {.bit = 0x4U, .index = 10},
	[3] = {.bit = 0x8U, .index = 17},
	[4] = {.bit = 0x10U, .index = 36},
	[5] = {.bit = 0x20U, .index = 28},
	[6] = {.bit = 0x40U, .index = 24},
	[7] = {.bit = 0x80U, .index = 21},
	[8] = {.bit = 0x100U, .index = 25},
	[9] = {.bit = 0x200U, .index = 22},

   [10] = {.bit = 0x400U, .index = 45},
   [11] = {.bit = 0x800U, .index = 27},
   [12] = {.bit = 0x1000U, .index = 14},
   [13] = {.bit = 0x2000U, .index = 6},
   [14] = {.bit = 0x4000U, .index = 7},
   [15] = {.bit = 0x8000U, .index = 11},
   [16] = {.bit = 0x10000U, .index = 23},
   [17] = {.bit = 0x20000U, .index = 48},
   [18] = {.bit = 0x40000U, .index = 47},
   [19] = {.bit = 0x80000U, .index = 19},

   [20] = {.bit = 0x100000U, .index = 18},
   [21] = {.bit = 0x200000U, .index = 43},
   [22] = {.bit = 0x400000U, .index = 9},
   [23] = {.bit = 0x800000U, .index = 41},
   [24] = {.bit = 0x1000000U, .index = 46},
   [25] = {.bit = 0x2000000U, .index = 40},
   [26] = {.bit = 0x4000000U, .index = 44},
   [27] = {.bit = 0x8000000U, .index = 30},
   [28] = {.bit = 0x10000000U, .index = 35},
   [29] = {.bit = 0x20000000U, .index = 31},

   [30] = {.bit = 0x40000000U, .index = 13},
   [31] = {.bit = 0x80000000U, .index = 20},
   [32] = {.bit = 0x100000000U, .index = 39},
   [33] = {.bit = 0x200000000U, .index = 34},
   [34] = {.bit = 0x400000000U, .index = 33},
   [35] = {.bit = 0x800000000U, .index = 38},
   [36] = {.bit = 0x1000000000U, .index = 2},
   [37] = {.bit = 0x2000000000U, .index = 26},
   [38] = {.bit = 0x4000000000U, .index = 32},
   [39] = {.bit = 0x8000000000U, .index = 3},

   [40] = {.bit = 0x10000000000U, .index = 16},
   [41] = {.bit = 0x20000000000U, .index = 12},
   [42] = {.bit = 0x40000000000U, .index = 8},
   [43] = {.bit = 0x80000000000U, .index = 4},
   [44] = {.bit = 0x100000000000U, .index = 1},
   [45] = {.bit = 0x200000000000U, .index = 5},
   [46] = {.bit = 0x400000000000U, .index = 29},
   [47] = {.bit = 0x800000000000U, .index = 42},
};

uint64 remapLED(uint16 i)
{
   uint16 index;
   uint64 mask = 0U;
   boolean found = FALSE;
   // Debug
   //return i;
   /* Find where i is mapped. */
   for (index = 0U; index < (sizeof(remapTable)/sizeof(remapTable[0])); index++)
   {
	   if (remapTable[index].index == i)
	   {
		   found = TRUE;
		   mask = remapTable[index].bit;
		   break;
	   }
   }
   if (found)
   {
	   return mask;
   }
   else
   {
	   return 0x0;	   
   }
}
 


boolean getLEDState(uint64 pattern, uint16 index)
{
	uint64 mask = ((uint64)1 << index);
	return (pattern & mask) == (mask);
}

void updateLEDs(uint64 pattern){
	uint16 i;
	uint64 newMask;
	for (i = 1; i <= 48; i++){
        newMask = remapLED(i) & pattern;
		setLED(i - 1U, getLEDState(newMask, i - 1U));
	}
}

void waitForButton(void)
{
	while (!isButtonPressed())
	{
		;
	}
	while(isButtonPressed())
	{
		;
	}
}

void runTheTree(void){
	uint64 ledPattern;
	uint64 lastPattern;
	uint64 starPattern;
	uint16 i;

    for (i = 0; i < getNbrOfLEDs(); i++)
	{
       setLED(i, FALSE);
	}

	long_delay();

    for (i = 0; i < getNbrOfLEDs(); i++)
	{
       setLED(i, TRUE);
	   //waitForButton();
	}
	long_delay();


	/* Debug pattern */
	updateLEDs(0x0);
	//waitForButton();
	
	for (ledPattern = 1; ledPattern != LAST_BIT; ledPattern <<= 1){
		updateLEDs(ledPattern);
		//waitForButton();
		medium_delay();
	}


	/* Alter every second led... */
	for (i = 0; i < 10; i++){
		ledPattern = 0x5555555555555555;
		updateLEDs(ledPattern);
		medium_delay();
		ledPattern = 0xAAAAAAAAAAAAAAAA;
		updateLEDs(ledPattern);
		medium_delay();
	}

	/* Move LED to the top... */
	for (ledPattern = 1; ledPattern != LAST_BIT; ledPattern <<= 1){
		updateLEDs(ledPattern);
		medium_delay();
	}

	/* Fill all by running through while rotating star.. */
	ledPattern = FIRST_TREE_BIT;
	lastPattern = FIRST_TREE_BIT;
	starPattern = FIRST_STAR_BIT;
	while (ledPattern != TREE_BITS){
		/* Tree */
		ledPattern =  (ledPattern << 1) | lastPattern;
        lastPattern = ledPattern;
        /* Star*/
        starPattern <<= 1;
        if (starPattern == (LAST_STAR_BIT << 1)){
        	starPattern = FIRST_STAR_BIT;
        }
        updateLEDs(ledPattern | starPattern);
        medium_delay();
	}
}

void mainFunction(void)
{
    initLEDs();
	initButton();
	while(1)
	{
		runTheTree();
	}
}