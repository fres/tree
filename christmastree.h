/*
 * christmastree.h
 *
 *  Created on: 10 dec 2014
 *      Author: Fredrik
 */

#ifndef CHRISTMASTREE_H_
#define CHRISTMASTREE_H_

#include "types.h"
void runTheTree(void);
void mainFunction(void);
void updateLEDs(uint64 pattern);
boolean getLEDState(uint64 pattern, uint16 index);
uint64 remapLED(uint16 i);

#endif /* CHRISTMASTREE_H_ */
